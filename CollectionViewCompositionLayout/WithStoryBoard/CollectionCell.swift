//
//  CollectionCell.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import UIKit

class CollectionCell: UICollectionViewCell {
	static let reuseIdentifier = "CollectionCell"
	
	@IBOutlet weak var imageView: UIImageView!
	
	func configure(image: UIImage?) {
		imageView.image = image
	}
}

