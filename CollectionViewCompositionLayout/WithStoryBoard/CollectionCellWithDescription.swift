//
//  CollectionCellWithDescription.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import UIKit

class CollectionCellWithDescription: UICollectionViewCell {
	static let reuseIdentifier = "CollectionCellWithDescription"
	
	@IBOutlet weak var subContentView: UIView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	
	func configure(with item: ImageFull) {
		nameLabel.text = item.description
		imageView.image = item.image
	}
}
