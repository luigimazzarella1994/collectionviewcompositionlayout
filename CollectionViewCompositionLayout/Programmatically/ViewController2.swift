//
//  ViewController2.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import UIKit

class ViewControlle2: UIViewController {
	private let activityIndicator = UIActivityIndicatorView(style: .large)
	private let sections: [Section] = [ .horizontal, .vertical]
	private var pngImages: [ImagePng] = []
	private var fullImages: [ImageFull] = []
	
	private lazy var collectionView: UICollectionView = {
		let cv = UICollectionView(frame: .zero, collectionViewLayout: createCompositionLayout())
		cv.delegate = self
		cv.dataSource = self
		cv.translatesAutoresizingMaskIntoConstraints = false
		cv.register(CollectionCell2.self, forCellWithReuseIdentifier: CollectionCell2.reuseIdentifier)
		cv.register(CollectionCellWithDescription2.self, forCellWithReuseIdentifier: CollectionCellWithDescription2.reuseIdentifier)
		return cv
	}()
	
	private func createCompositionLayout() -> UICollectionViewCompositionalLayout {
		return UICollectionViewCompositionalLayout(sectionProvider: { sectionIndex, collectioViewEnviroment in
			let section = self.sections[sectionIndex]
			switch section {
				case .horizontal:
					let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1),
																		heightDimension: .fractionalHeight(1)))
					let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .absolute(200),
																					 heightDimension: .absolute(200)),
																   subitems: [item])
					let section = NSCollectionLayoutSection(group: group)
					section.interGroupSpacing = 10
					section.contentInsets = .init(top: 0, leading: 10, bottom: 0, trailing: 10)
					section.orthogonalScrollingBehavior = .continuous
					return section
				case .vertical:
					let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1),
																		heightDimension: .fractionalHeight(1)))
					let group = NSCollectionLayoutGroup.vertical(layoutSize: .init(widthDimension: .fractionalWidth(1),
																				   heightDimension: .absolute(400)),
																 subitems: [item])
					let section = NSCollectionLayoutSection(group: group)
					section.interGroupSpacing = 7
					section.contentInsets = .init(top: 0, leading: 0, bottom: 10, trailing: 0)
					
					return section
			}
		})
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupLayout()
		getData()
	}
	
	private func setupLayout(){
		view.addSubview(collectionView)
		NSLayoutConstraint.activate([
			collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
			collectionView.topAnchor.constraint(equalTo: view.topAnchor)
		])
		view.addSubview(activityIndicator)
		activityIndicator.translatesAutoresizingMaskIntoConstraints =  false
		activityIndicator.hidesWhenStopped = true
		NSLayoutConstraint.activate([
			activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
			activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
		])
	}
	
	private func getData(){
		activityIndicator.startAnimating()
		let downloadContentQueue = DispatchQueue.global()
		let group = DispatchGroup()
		
		downloadContentQueue.async(group: group) {
			sleep(2)
			self.pngImages = [
				ImagePng(image: UIImage(named: "testPng1") , description: "Immagine Png"),
				ImagePng(image: UIImage(named: "testPng2") , description: "Immagine Png"),
				ImagePng(image: UIImage(named: "testPng3") , description: "Immagine Png"),
				ImagePng(image: UIImage(named: "testPng4") , description: "Immagine Png")
			]
		}
		
		downloadContentQueue.async(group: group) {
			sleep(4)
			self.fullImages = [
				ImageFull(image: UIImage(named: "test1"), description: "Wow"),
				ImageFull(image: UIImage(named: "test2"), description: "Wow"),
				ImageFull(image: UIImage(named: "test6"), description: "Wow"),
			]
		}
		
		group.notify(queue: .main) {
			self.collectionView.reloadData()
			self.activityIndicator.stopAnimating()
		}
	}
}

extension ViewControlle2: UICollectionViewDelegate, UICollectionViewDataSource {
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		sections.count
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if section == 0 {
			return pngImages.count
		} else {
			return fullImages.count
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		if indexPath.section == 0 {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCell.reuseIdentifier, for: indexPath) as! CollectionCell2
			cell.configure(image: pngImages[indexPath.item].image)
			return cell
		} else {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellWithDescription.reuseIdentifier, for: indexPath) as! CollectionCellWithDescription2
			cell.configure(with: fullImages[indexPath.item])
			return cell
		}
	}
}
