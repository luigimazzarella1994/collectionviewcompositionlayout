//
//  CollectionCell2.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import UIKit


class CollectionCell2: UICollectionViewCell {
	static let reuseIdentifier = "CollectionCell"
	
	private let imageView = UIImageView()
	
	func configure(image: UIImage?) {
		imageView.image = image
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupContentUI()
	}
	
	private func setupContentUI(){
		contentView.addSubview(imageView)
		imageView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
			imageView.topAnchor.constraint(equalTo: contentView.topAnchor)
		])
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}


