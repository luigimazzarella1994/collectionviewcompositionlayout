//
//  CollectionCellWithDescription2.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import UIKit

class CollectionCellWithDescription2: UICollectionViewCell {
	static let reuseIdentifier = "CollectionCellWithDescription"
	
	private let subContentView = UIView()
	private let nameLabel = UILabel()
	private let imageView = UIImageView()
	private var hasShadow = false
	
	func configure(with item: ImageFull) {
		nameLabel.text = item.description
		imageView.image = item.image
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupContentUI()
	}
	
	private func setupContentUI(){
		
		contentView.addSubview(subContentView)
		subContentView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			subContentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
			subContentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
			subContentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
			subContentView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10)
		])
		
		subContentView.addSubview(imageView)
		imageView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			imageView.leadingAnchor.constraint(equalTo: subContentView.leadingAnchor),
			imageView.trailingAnchor.constraint(equalTo: subContentView.trailingAnchor),
			imageView.heightAnchor.constraint(equalTo: subContentView.heightAnchor, multiplier: 0.8),
			imageView.topAnchor.constraint(equalTo: subContentView.topAnchor)
		])
		subContentView.addSubview(nameLabel)
		nameLabel.translatesAutoresizingMaskIntoConstraints = false
		nameLabel.textAlignment = .center
		nameLabel.font = .boldSystemFont(ofSize: 24)
		NSLayoutConstraint.activate([
			nameLabel.leadingAnchor.constraint(equalTo: subContentView.leadingAnchor),
			nameLabel.trailingAnchor.constraint(equalTo: subContentView.trailingAnchor),
			nameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 7)
		])
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		if !hasShadow {
			hasShadow = true
			subContentView.layer.cornerRadius = 5
			subContentView.layer.shadowOffset = .init(width: 2, height: 2)
			subContentView.layer.shadowRadius = 5
			subContentView.layer.shadowOpacity = 0.2
			subContentView.layer.shadowColor = UIColor.systemGray.cgColor
			subContentView.layer.shadowPath = UIBezierPath(rect: subContentView.bounds).cgPath
		}
	}
}
