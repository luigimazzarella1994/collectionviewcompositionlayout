//
//  LayoutItem.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import UIKit

protocol LayoutItem {
	var image: UIImage? { get }
	var description: String  { get }
}

struct ImagePng: LayoutItem {
	let image: UIImage?
	let description: String
}

struct ImageFull: LayoutItem {
	let image: UIImage?
	let description: String
}
