//
//  Sections.swift
//  CollectionViewCompositionLayout
//
//  Created by Macbook on 28/10/22.
//

import Foundation

enum Section {
	case horizontal, vertical
}
